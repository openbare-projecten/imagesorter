package nl.robertvankammen.imagesorter;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;


@Component
public class ImageSorterDatePhotoTaken {

    @Value("${scanlocatie}")
    private String locationPictures;
    private FileMoveUtils fileMoveUtils;
    private ArrayList<String> extentionList = new ArrayList<>();

    public ImageSorterDatePhotoTaken(FileMoveUtils fileMoveUtils) {
        this.fileMoveUtils = fileMoveUtils;
        extentionList.add("jpg");
        extentionList.add("png");
        extentionList.add("jpeg");
    }

    public void scan() {
        File baseLocation = new File(locationPictures);
        recursiveScanDirectorys(baseLocation);
    }

    private void recursiveScanDirectorys(File folder) {
        Arrays.stream(folder.listFiles()).forEach(file -> {
            if (file.isDirectory()) {
                recursiveScanDirectorys(file);
            } else {
                sortImage(file);
            }
        });
    }

    private void sortImage(File file) {
        try {
            int day = 0;
            int month = 0;
            int year = 0;
            LocalDateTime datum = getPhotoTakenDate(file);
            if (datum != null) {
                day = datum.getDayOfMonth();
                month = datum.getMonthValue();
                year = datum.getYear();
            }
            fileMoveUtils.moveFile(day, month, year, file);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public LocalDateTime getPhotoTakenDate(File file) {
        String extension = FilenameUtils.getExtension(file.getPath());
        if (extentionList.contains(extension.toLowerCase())) {
            try {
                Metadata metadata = ImageMetadataReader.readMetadata(file);
                final ExifSubIFDDirectory exifSubIFDDirectory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
                if (exifSubIFDDirectory != null && exifSubIFDDirectory.getDateOriginal() != null) {
                    return LocalDateTime.ofInstant(exifSubIFDDirectory.getDateOriginal().toInstant(), ZoneId.systemDefault());
                }
            } catch (Exception e) {
                System.out.println(file.getPath());
                e.printStackTrace();
            }
        }
        return null;
    }
}
