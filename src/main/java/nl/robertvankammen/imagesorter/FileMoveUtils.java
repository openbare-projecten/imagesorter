package nl.robertvankammen.imagesorter;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileMoveUtils {

    @Value("${scanlocatie}")
    private String locationPictures;

    @Value("${newlocation}")
    private String newPictureLocation;

    public void moveFile(int day, int month, int year, File file) {
        try {

            File dest = new File(newPictureLocation + "/" + year + "/" + month + "/" + day);
            dest.mkdirs();
            dest = new File(newPictureLocation + "/" + year + "/" + month + "/" + day + "/" + file.getName());
            FileUtils.copyFile(file, dest);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
