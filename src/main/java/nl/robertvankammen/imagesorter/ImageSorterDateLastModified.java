package nl.robertvankammen.imagesorter;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


@Component
public class ImageSorterDateLastModified {

    @Value("${scanlocatie}")
    private String locationPictures;
    private FileMoveUtils fileMoveUtils;

    public ImageSorterDateLastModified(FileMoveUtils fileMoveUtils) {
        this.fileMoveUtils = fileMoveUtils;

    }

    public void scan() {
        File baseLocation = new File(locationPictures);
        recursiveScanDirectorys(baseLocation);
    }

    private void recursiveScanDirectorys(File folder) {
        Arrays.stream(folder.listFiles()).forEach(file -> {
            if (file.isDirectory()) {
                recursiveScanDirectorys(file);
            } else {
                sortImage(file);
            }
        });
    }

    private void sortImage(File file) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date theDate = sdf.parse(sdf.format(file.lastModified()));

            Calendar myCal = new GregorianCalendar();

            myCal.setTime(theDate);

            int day = myCal.get(Calendar.DAY_OF_MONTH);
            int month = myCal.get(Calendar.MONTH) + 1;
            int year = myCal.get(Calendar.YEAR);
            fileMoveUtils.moveFile(day, month, year, file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
