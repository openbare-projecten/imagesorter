package nl.robertvankammen.imagesorter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ImagesorterApplication {

    public static void main(String[] args) {
        ApplicationContext app = SpringApplication.run(ImagesorterApplication.class);
        //ImageSorterDateLastModified scanner = app.getBean(ImageSorterDateLastModified.class);
        ImageSorterDatePhotoTaken scanner = app.getBean(ImageSorterDatePhotoTaken.class);

        scanner.scan();
    }
}
